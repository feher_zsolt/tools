﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace NUnitTestListMaker
{
    class Program
    {
        const string TestCaseElementName = "test-case";
        const string SuccessAttributeName = "success";
        const string TestFailedValue = "False";
        const string NameAttributeName = "name";

        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: {0} <NUnit result file> <output test list>", Assembly.GetExecutingAssembly().GetName().Name);
                return;
            }

            try
            {
                XDocument doc = XDocument.Parse(File.ReadAllText(args[0]));

                var failedTests = doc.Descendants(TestCaseElementName)
                    .Where(node => IsFailedTestNode(node))
                    .Select(node => node.Attribute(NameAttributeName).Value);

                File.WriteAllLines(args[1], failedTests);

                Console.WriteLine("Test list written to {0}, use /runlist:<output test list>.", args[1]);

            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occured.");
                Console.WriteLine(ex);
            }
        }

        private static bool IsFailedTestNode(XElement xElement)
        {
            if (xElement == null)
                return false;

            var attribute = xElement.Attribute(SuccessAttributeName);
            if (attribute == null)
                return false;

            return attribute.Value == TestFailedValue;
        }
    }
}
